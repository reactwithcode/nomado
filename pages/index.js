import Head from 'next/head'
import navbar from './components/navbar';
import header from './components/header';
import main from './components/main';
import footer from './components/footer';

export default function Home() {
  return (
    <div className="flex flex-col items-center justify-center min-h-screen py-2">
      <navbar/>
      <header/>
      <main/>
      <footer/>
    </div>
  )
}
